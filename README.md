# Product-Filter
Is a REST Api that lists products with search filter possibility.
The product catalogue is loaded from a csv file into an in-memory database.

The data is loaded at application startup. A scheduler is programmed to load/update the data periodically. The database and the caches are cleared before the updates.

## Approach
The solution presented in this project does not pretend to be the most efficient for this simple task but rather a showcase of some technologies more suitable for more complex use cases.
* Batch job is used to read the data from a csv file, transform the data and store it in to the database.
* A Scheduled task triggers the job periodically. The database is cleared before the job runs.
* Results of search are cached. The cache is evicted after updates.

## Tech showcase
* Spring boot
* Spring Batch
* Spring data jpa, Hibernate, in-memory database H2
* Spring Rest
* Spring cron scheduler
* Caffeine cache
* Lombok
* Docker
* OpenAPI Rest documentation (Swagger)
* Modelmapper

## Prerequisits
- Java 13+
- Maven 3.2+
- Git

## Use guide
#### Clone the repository to your local computer 
```
git clone https://bitbucket.org/mimisa/product-filter.git
```
#### Build the project 
```
mvn clean install
```

#### Run the application locally with maven
```
mvn clean spring-boot:run
```
#### Run the application locally with docker
- Build the docker image
```
docker build -t product-filter .

```
- Run a docker container from the image
```
docker run -d -p 8080:8080 product-filter

```
- Stop the running Docker container
```
docker stop [CONTAINER_ID]
```
CONTAINER_ID you get by running
```
docker ps
```


#### API documentation
```
    http://localhost:8080/v3/api-docs/

    http://localhost:8080/swagger-ui.html
```

#### Use it   

```
List all the products
    GET http://localhost:8080/products/all

Filter the products
 GET http://localhost:8080/products?[SEARCH_FILTER]
i.e. http://localhost:8080/products?type=phone&city=Karlskrona&max_price=100
```
```
SEARCH_FILTER conntains 1 or more query parameters from following:
type, min_price, max_price, city,color, gb_limit_min, gb_limit_max
```







