package com.code.home.controller;

import com.code.home.model.ProductDto;
import com.code.home.model.Query;
import com.code.home.model.QueryParams;
import com.code.home.service.InvalidQueryException;
import com.code.home.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@Slf4j
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;
    private final ObjectMapper objectMapper;

    @GetMapping("/products/all")
    @ResponseBody
    public List<ProductDto> allProducts() {
        return productService.allProducts();
    }

    @GetMapping("/products")
    @ResponseBody
    public List<ProductDto> products(@RequestParam Map<String, String> allParams) {
        log.debug("Request params {}", allParams);

        Map<String, String> query = allParams.entrySet()
                .stream()
                .filter(e -> EnumSet.allOf(QueryParams.class).contains(e.getKey()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        Query q = objectMapper.convertValue(allParams, Query.class);
        return productService.products(q);
    }

    @ExceptionHandler(InvalidQueryException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handle(InvalidQueryException e) {
        return "Invalid search filter: " + e.getMessage();
    }

}
