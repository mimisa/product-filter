package com.code.home;

import com.code.home.batch.ProductFieldSetMapper;
import com.code.home.batch.ProductItemProcessor;
import com.code.home.batch.ProductItemWriter;
import com.code.home.model.ProductEntity;
import com.code.home.model.Product;
import org.modelmapper.ModelMapper;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.persistence.EntityManagerFactory;

@EnableBatchProcessing
@EnableCaching
@EnableScheduling
@Configuration
public class AppConfig {
    private static final String PRODUCT_CATALOG_JOB = "PRODUCT_CATALOG_JOB";
    private static final String PRODUCT_CATALOG_STEP = "PRODUCT_CATALOG_STEP";

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    EntityManagerFactory entityManagerFactory;

    @Value("classpath:data.csv")
    private Resource productData;

    @Bean
    @Qualifier(PRODUCT_CATALOG_JOB)
    public Job productCatalogJob() {
        return jobBuilderFactory.get(PRODUCT_CATALOG_JOB)
                .start(productCatalogStep())
                .build();
    }

    @Bean
    public Step productCatalogStep() {
        return stepBuilderFactory.get(PRODUCT_CATALOG_STEP)
                .<Product, ProductEntity>chunk(20)
                .reader(productReader())
                .processor(new ProductItemProcessor())
                .writer(new ProductItemWriter(entityManagerFactory))
                .build();
    }

    @Bean
    public FlatFileItemReader<Product> productReader() {
        return new FlatFileItemReaderBuilder<Product>()
                .name("ProductsFromCsv")
                .resource(productData)
                .delimited()
                .delimiter(",")
                .names(new String[]{"type", "properties", "price", "adress"})
                .fieldSetMapper(new ProductFieldSetMapper())
                .linesToSkip(1)
                .build();
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

}
