package com.code.home.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Value;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Value
@Getter
@Builder(builderClassName = "Builder")
public class Product {
    private String type;
    private BigDecimal price;
    private Address storeAddress;
    private Map<String,String> properties;

    private Map<String, String> attribute = new HashMap<>();

}
