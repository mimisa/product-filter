package com.code.home.model;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderClassName = "Builder")
public class ProductDto {

    private String type;
    private String color;
    private Integer gbLimit;
    private BigDecimal price;
    private String street;
    private String city;

}
