package com.code.home.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;
import lombok.Value;

@Getter
@Value
@JsonDeserialize(builder = Query.Builder.class)
public class Query {
    private final String type;
    private final String min_price;
    private final String max_price;
    private final String city;
    private final String color;
    private final String gb_limit_min;
    private final String gb_limit_max;

    public static Builder builder(){
        return new Builder();
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Builder {
        private String type;
        @JsonProperty("min_price")
        private String min_price;
        @JsonProperty("max_price")
        private String max_price;
        @JsonProperty("city")
        private String city;
        @JsonProperty("color")
        private String color;
        @JsonProperty("gb_limit_min")
        private String gb_limit_min;
        @JsonProperty("gb_limit_max")
        private String gb_limit_max;

        Builder() {}

        public Builder type(String type) {
            this.type = type;
            return Builder.this;
        }

        public Builder min_price(String min_price) {
            this.min_price = min_price;
            return Builder.this;
        }

        public Builder max_price(String max_price) {
            this.max_price = max_price;
            return Builder.this;
        }

        public Builder city(String city) {
            this.city = city;
            return Builder.this;
        }

        public Builder color(String color) {
            this.color = color;
            return Builder.this;
        }

        public Builder gb_limit_min(String gb_limit_min) {
            this.gb_limit_min = gb_limit_min;
            return Builder.this;
        }

        public Builder gb_limit_max(String gb_limit_max) {
            this.gb_limit_max = gb_limit_max;
            return Builder.this;
        }

        public Query build() {
            return new Query(this.type, this.min_price, this.max_price, this.city, this.color, this.gb_limit_min, this.gb_limit_max);
        }
    }

    public boolean isEmpty() {
        return
                (type == null|| type.isEmpty())
                && (city == null|| city.isEmpty())
                && (color == null|| color.isEmpty())
                && (min_price == null|| min_price.isEmpty())
                && (max_price == null|| max_price.isEmpty())
                && (gb_limit_min == null|| gb_limit_min.isEmpty())
                && (gb_limit_max == null|| gb_limit_max.isEmpty());
    }

}
