package com.code.home.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Builder;
import lombok.Getter;
import lombok.Value;

@Value
@Getter
@JsonDeserialize(builder = Product.Builder.class)
@Builder(builderClassName = "Builder")
public class Address {
    private String street;
    private String city;
}
