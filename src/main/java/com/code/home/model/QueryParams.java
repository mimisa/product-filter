package com.code.home.model;

public enum QueryParams {
    type,
    min_price,
    max_price,
    city,
    color,
    gb_limit_min,
    gb_limit_max;
}
