package com.code.home.batch;

import com.code.home.model.Address;
import com.code.home.model.Product;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import java.util.Map;

public class ProductFieldSetMapper implements FieldSetMapper<Product> {

    @Override
    public Product mapFieldSet(FieldSet fieldSet) throws BindException {
        String[] properties = fieldSet.readString("properties").split(":");
        String[] address = fieldSet.readString("adress").trim().split(",");
        return Product.builder()
                .type(fieldSet.readString("type"))
                .properties(Map.of(properties[0],properties[1]))
                .price(fieldSet.readBigDecimal("price"))
                .storeAddress(Address.builder()
                        .street(address[0].trim())
                        .city(address[1].trim()).build())
                .build();
    }


}
