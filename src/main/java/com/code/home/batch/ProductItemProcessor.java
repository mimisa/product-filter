package com.code.home.batch;

import com.code.home.model.ProductEntity;
import com.code.home.model.Product;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ProductItemProcessor implements ItemProcessor<Product, ProductEntity> {

    @Override
    public ProductEntity process(Product product) throws Exception {

        Integer gbLimit =  Optional.ofNullable(product.getProperties().get("gb_limit"))
                .map(Integer::valueOf).orElse(null);
        return ProductEntity.builder()
                .type(product.getType())
                .color(product.getProperties().get("color"))
                .gbLimit(gbLimit)
                .price(product.getPrice())
                .street(product.getStoreAddress().getStreet())
                .city(product.getStoreAddress().getCity())
                .build();
    }
}
