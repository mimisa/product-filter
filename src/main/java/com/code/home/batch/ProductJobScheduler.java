package com.code.home.batch;

import com.code.home.repository.ProductRepository;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Builder
@Slf4j
public class ProductJobScheduler {

    private final List<Job> jobs;
    private final JobLauncher jobLauncher;
    private final CacheManager cacheManager;
    private final ProductRepository repository;


    @Scheduled(cron = "${api.product.batch.scheduling}")
    public void runJob() throws Exception {
        clearTable();
        jobs.forEach(job -> {
            JobExecution execution = null;
            try {
                execution = jobLauncher.run(
                        job,
                        new JobParametersBuilder().addLong("JOB_EXECUTION_ " + job.getName(), System.nanoTime()).toJobParameters()
                );
            } catch (Exception e) {
                log.error("Failed to run job {} ", job.getName(), e);
                throw new RuntimeException("Failed to run job " + job.getName(), e);
            }
            log.info("Exit status: {}", execution.getStatus());
        });
        evictAllCache();
    }

    private void clearTable() {
        log.info("Clearing the database table PRODUCTS before updateing with new data from batch job.");
        repository.deleteAll();
        log.info("Clearing the database table PRODUCTS done!");
    }
    private void evictAllCache() {
        log.info("Clearing caches after new batch job run successfully.");
        cacheManager.getCacheNames()
                .forEach(name -> cacheManager.getCache(name).clear());
    }
}
