package com.code.home.batch;

import lombok.RequiredArgsConstructor;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

@RequiredArgsConstructor
public class ProductItemWriter<T> extends JpaItemWriter<T> {

    EntityManagerFactory entityManagerFactory;

    public ProductItemWriter(EntityManagerFactory entityManagerFactory) {
        setEntityManagerFactory(entityManagerFactory);
    }

    @Override
    protected void doWrite(EntityManager entityManager, List<? extends T> items) {
        super.doWrite(entityManager, items);
    }


}
