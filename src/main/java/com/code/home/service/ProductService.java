package com.code.home.service;

import com.code.home.model.ProductDto;
import com.code.home.model.ProductEntity;
import com.code.home.model.Query;
import com.code.home.model.QueryParams;
import com.code.home.repository.ProductRepository;
import com.code.home.repository.ProductSpecification;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Builder
@Component
@Slf4j
public class ProductService {

    private final ProductRepository productRepository;
    private final ModelMapper modelMapper;

    @Cacheable(value = "products")
    public List<ProductDto> allProducts() {
        return productRepository.findAll().stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Cacheable(value = "products", key= "#query")
    public List<ProductDto> products(Query query) {
        if (query == null || query.isEmpty()) {
            String values = EnumSet.allOf(QueryParams.class).stream().map(Object::toString).collect(Collectors.joining(","));
            log.error("Search query is not valid: {}", query== null? null: query.toString());
            throw new InvalidQueryException(String.format("search query must comtain one or more criteria: %s", values));
        }

        ProductSpecification filter = new ProductSpecification(query);

        List<ProductEntity> result = productRepository.findAll(filter);

        return result.stream()
                .map(this::toDto)
                .collect(Collectors.toList());

    }

    private ProductDto toDto(ProductEntity source) {
        ProductDto dto = modelMapper.map(source, ProductDto.class);
        return dto;
    }
}
