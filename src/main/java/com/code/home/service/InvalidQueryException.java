package com.code.home.service;

public class InvalidQueryException extends RuntimeException {

        public InvalidQueryException(String message, Throwable cause) {
            super(message, cause);
        }

        public InvalidQueryException(String message) {
            super(message);
        }

}
