package com.code.home.repository;

import com.code.home.model.ProductEntity;
import com.code.home.model.Query;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Slf4j
public class ProductSpecification implements Specification<ProductEntity> {

    private final Query query;

    @Override
    public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        if (query.getCity() != null) {
            predicates.add(criteriaBuilder.equal(root.get("city"), query.getCity()));
        }
        if (query.getType() != null) {
            predicates.add(criteriaBuilder.equal(root.get("type"), query.getType()));
        }
        if (query.getColor() != null) {
            predicates.add(criteriaBuilder.equal(root.get("color"), query.getColor()));
        }

        if (query.getMin_price() != null) {
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("price"), new BigDecimal(query.getMin_price())));
        }

        if (query.getMax_price() != null) {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("price"), new BigDecimal(query.getMax_price())));
        }
        if (query.getGb_limit_min() != null) {
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("gbLimit"), Integer.valueOf(query.getGb_limit_min())));
        }

        if (query.getGb_limit_max() != null) {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("gbLimit"), Integer.valueOf(query.getGb_limit_max())));
        }


        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }
}
