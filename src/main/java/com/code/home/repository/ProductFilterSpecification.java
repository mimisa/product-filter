package com.code.home.repository;

import com.code.home.model.ProductEntity;
import com.code.home.model.Query;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import static java.util.Objects.isNull;
import static org.springframework.data.jpa.domain.Specification.where;

@Slf4j
public class ProductFilterSpecification {

    public static Specification<ProductEntity> hasType(String type) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            if (isNull(type)) {
                return criteriaBuilder.conjunction();
            }
            return criteriaBuilder.equal(root.get("type"), type);
        };


    }

    public static Specification<ProductEntity> hasColor(String color) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            if (isNull(color)) {
                return criteriaBuilder.conjunction();
            }
            return criteriaBuilder.equal(root.get("color"), color);
        };


    }

    public static Specification<ProductEntity> isColor(String color) {
        log.debug("COLOR {}", color);
        return new Specification<ProductEntity>() {
            public Predicate toPredicate(Root<ProductEntity> root, CriteriaQuery<?> query,
                                         CriteriaBuilder builder) {
                if (isNull(color)) {
                    return builder.conjunction();
                }
                return builder.equal(root.get("color"), color);
            }
        };
    }

    public static Specification<ProductEntity> isCity(String city) {
        log.debug("CITY {}", city);
        return new Specification<ProductEntity>() {
            public Predicate toPredicate(Root<ProductEntity> root, CriteriaQuery<?> query,
                                         CriteriaBuilder builder) {
                if (isNull(city)) {
                    return builder.conjunction();
                }
                return builder.equal(root.get("city"), city);
            }
        };
    }
    public static Specification<ProductEntity> hasCity(String city) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            if (isNull(city)) {
                return criteriaBuilder.conjunction();
            }
            return criteriaBuilder.equal(root.get("city"), city);
        };
    }

    public static Specification<ProductEntity> priceLessThanOrEqual(String min_price) {
        return (Specification<ProductEntity>) (root, criteriaQuery, criteriaBuilder) -> {
            if (isNull(min_price)) {
                return criteriaBuilder.conjunction();
            }
            return criteriaBuilder.lessThanOrEqualTo(root.get("price"), min_price);

        };
    }

    public static Specification<ProductEntity> priceGreaterThanOrEqual(String max_price) {
        return (Specification<ProductEntity>) (root, criteriaQuery, criteriaBuilder) -> {
            if (isNull(max_price)) {
                return criteriaBuilder.conjunction();
            }
            return criteriaBuilder.greaterThanOrEqualTo(root.get("price"), max_price);

        };
    }

    public static Specification<ProductEntity> gbLimitLessThanOrEqual(String gb_limit_min) {
        log.debug("gb_limit_min", gb_limit_min);
        return (Specification<ProductEntity>) (root, criteriaQuery, criteriaBuilder) -> {
            if (isNull(gb_limit_min)) {
                return criteriaBuilder.conjunction();
            }
            return criteriaBuilder.lessThanOrEqualTo(root.get("gbLimit"), gb_limit_min);

        };
    }

    public static Specification<ProductEntity> gbLimitGreaterThanOrEqual(String gb_limit_max) {
        return (Specification<ProductEntity>) (root, criteriaQuery, criteriaBuilder) -> {
            if (isNull(gb_limit_max)) {
                return criteriaBuilder.conjunction();
            }
            return criteriaBuilder.greaterThanOrEqualTo(root.get("price"), gb_limit_max);

        };
    }


    public static Specification<ProductEntity> getFilter(Query search) {
        log.debug("search {}", search);
        if (isNull(search)) return null;
        return (root, query, criteriaBuilder) -> {
            return where(hasType(search.getType()))
                   .and(hasColor(search.getColor()))
//                    .and(hasCity(search.getCity()))
//                    .and(priceLessThanOrEqual(search.getMin_price()))
//                    .and(priceGreaterThanOrEqual(search.getMax_price()))
//                    .and(gbLimitLessThanOrEqual(search.getGb_limit_min()))
//                    .and(gbLimitGreaterThanOrEqual(search.getGb_limit_max()))
                    .toPredicate(root, query, criteriaBuilder);

        };
    }

//    public static Specification<ProductEntity> search(Query query) {
//        return (Specification<ProductEntity>) (root, criteriaQuery, criteriaBuilder) -> {
//
//            Predicate and = criteriaBuilder.and(
//                    hasType(query.getType()),
//                    hasColor(query.getColor()),
//                    priceLessThanOrEqual(query.getMin_price()),
//                    priceGreaterThanOrEqual(query.getMax_price()),
//                    gbLimitLessThanOrEqual(query.getGb_limit_min()),
//                    gbLimitGreaterThanOrEqual(query.getGb_limit_max()),
//                    hasCity(query.getCity())
//            );
//            return and;
//
//        };
//    }

}

