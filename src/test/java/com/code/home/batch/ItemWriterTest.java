package com.code.home.batch;

import com.code.home.model.ProductEntity;
import com.code.home.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;

import javax.persistence.EntityManagerFactory;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@TestPropertySource(properties = {"spring.jpa.hibernate.ddl-auto=create-drop",})
public class ItemWriterTest {

    @Autowired
    EntityManagerFactory entityManagerFactory;

    @Autowired
    private ProductRepository productRepository;

    private ProductItemWriter<ProductEntity> itemWriter = new ProductItemWriter() {
        {setEntityManagerFactory(entityManagerFactory);}
    };

    @Test
    void testSusccessfulWriting() {

        ProductEntity product_one = new ProductEntity();
        ProductEntity product_two = new ProductEntity();

        itemWriter.setEntityManagerFactory(entityManagerFactory);
        itemWriter.write(List.of(product_one,product_two));

        assertEquals(2, productRepository.findAll().size());
    }
}
