package com.code.home.batch;

import com.code.home.model.ProductEntity;
import com.code.home.model.Address;
import com.code.home.model.Product;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ItemprocessorTests {

    ProductItemProcessor itemProcessor = new ProductItemProcessor();

    @Test
    public void testItemProcessor() throws Exception {

        Product product = Product.builder()
                .type("T")
                .price(new BigDecimal("240.00"))
                .properties(Map.of("color", "Blue"))
                .storeAddress(Address.builder().street("Street").city("City").build())
                .build();

        ProductEntity productEntity = itemProcessor.process(product);

        assertNotNull(productEntity);
        assertEquals(productEntity.getCity(), "City");
    }
}
