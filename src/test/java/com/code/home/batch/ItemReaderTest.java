package com.code.home.batch;

import com.code.home.batch.ProductFieldSetMapper;
import com.code.home.model.Product;
import org.junit.jupiter.api.Test;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.test.MetaDataInstanceFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ItemReaderTest {

    private FlatFileItemReader<Product> reader = new FlatFileItemReader<Product>();
    private Resource fileResource = new ClassPathResource("test.csv");
    private ExecutionContext executionContext = MetaDataInstanceFactory.createStepExecution().getExecutionContext();
    private static int EXPECTED_ITEMS = 4;

    @Test
    public void testSuccessfulReading() throws Exception {

        reader.setLineMapper(new DefaultLineMapper<Product>() {
            {
                setLineTokenizer(new DelimitedLineTokenizer(",") {{
                    setNames(new String[]{"type", "properties", "price", "adress"});
                }});
                setFieldSetMapper(new ProductFieldSetMapper());
            }
        });
        reader.setResource(fileResource);
        reader.setLinesToSkip(1);

        int count = 0;
        reader.update(executionContext);
        reader.open(executionContext);
        while (reader.read() != null) {
                count++;
            }
        reader.close();

        assertEquals(EXPECTED_ITEMS, count);
    }
}

