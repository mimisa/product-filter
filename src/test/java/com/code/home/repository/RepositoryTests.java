package com.code.home.repository;

import com.code.home.model.ProductEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;
import static org.junit.jupiter.api.Assertions.*;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.List;

@DataJpaTest
@TestPropertySource(properties = {"spring.jpa.hibernate.ddl-auto=create-drop",})
public class RepositoryTests {

        @Autowired
        private ProductRepository productRepository;

        @Autowired
        private DataSource dataSource;

        @Autowired
        private EntityManagerFactory entityManagerFactory;

        @Test
        void injectedComponentsAreNotNull() {
            assertNotNull(productRepository);
            assertNotNull(dataSource);
            assertNotNull(entityManagerFactory);
        }

        @Test
        void testDatabaseEmpty() {
            assertEquals(0, productRepository.findAll().size());
        }

        @Test
        void testDatabaseContainsElements() {
            ProductEntity product_one = new ProductEntity();
            ProductEntity product_two = new ProductEntity();
            productRepository.saveAll(List.of(product_one,product_two));
            assertEquals(2, productRepository.findAll().size());
        }

    }
