package com.code.home.service;


import com.code.home.model.ProductDto;
import com.code.home.model.Query;
import com.code.home.model.ProductEntity;
import com.code.home.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class ProductServiceTests {

    private final ProductRepository productRepository = mock(ProductRepository.class);
    private final ModelMapper modelMapper = new ModelMapper();

    private final ProductService productService = ProductService.builder().productRepository(productRepository).modelMapper(modelMapper).build();

    @Test
    public void whenFindAll_thenReturnProductList() {

        Query query = Query.builder().city("CITY").type("TYPE").build();
        List<ProductEntity> resultEntity = List.of(ProductEntity.builder().type("TYPE").city("CITY").build());
        List<ProductDto> expectedResult = List.of(ProductDto.builder().type("TYPE").city("CITY").build());

        when(productRepository.findAll(any(Specification.class))).thenReturn(resultEntity);

        List<?> result = productService.products(query);

        assertEquals(1, result.size());
        assertTrue(result.get(0) instanceof ProductDto);
        assertEquals("CITY", ((ProductDto) result.get(0)).getCity());
    }

    @Test
    public void throwException_when_empty_query() {
        Query query = Query.builder().build();
        assertThrows(InvalidQueryException.class, () -> productService.products(query));
    }

    @Test
    public void throwException_when_null_query() {
        assertThrows(InvalidQueryException.class, () -> productService.products(null));
    }
}
