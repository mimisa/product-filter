package com.code.home.controller;

import com.code.home.model.ProductDto;
import com.code.home.model.ProductEntity;
import com.code.home.repository.ProductRepository;
import com.code.home.service.InvalidQueryException;
import com.code.home.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;


public class ProductControllerTests {

    private MockMvc mockMvc;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private ProductController productController;

    private final ProductRepository productRepository = mock(ProductRepository.class);
    private final ModelMapper modelMapper = new ModelMapper();
    private final ProductService productService = ProductService.builder().productRepository(productRepository).modelMapper(modelMapper).build();

    private final static Map validQuery = Map.of("city", "CITY", "type", "TYPE");
    private final static Map invalidQuery = Map.of("boo", "CITY", "baa", "TYPE");

    @BeforeEach
    void setUp() {
        productController = new ProductController(productService, objectMapper);
        mockMvc = standaloneSetup(productController).setHandlerExceptionResolvers(new ExceptionHandlerExceptionResolver()).build();
    }

    @Test
    public void shouldReturnStatus_OK() throws Exception {

        List<ProductEntity> resultEntity = List.of(ProductEntity.builder().type("TYPE").city("CITY").build());
        List<ProductDto> result = productController.products(validQuery);

        when(productRepository.findAll(any(Specification.class))).thenReturn(resultEntity);

        MvcResult mvcResult = mockMvc.perform(get("/products?city=City&type=Type"))
                .andExpect(status().isOk())
                .andReturn();
        assertNotNull(result);
        assertTrue((mvcResult.getResponse().getContentAsString()).contains("CITY"));
    }
}